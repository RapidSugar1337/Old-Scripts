from tkinter.filedialog import asksaveasfile, askopenfile
from tkinter.messagebox import showerror
from tkinter import messagebox
import tkinter as tk
root = tk.Tk()
def save_as():
    t = asksaveasfile(mode = 'w', defaultextension='txt')
    data = text.get('1.0', 'end')
    try:
        t.write(data.rstrip())
    except Exception:
        pass
def open_file():
    global file_name
    t = askopenfile(mode = 'r')
    if t is None:
        return 0
    file_name = t.name
    data = t.read()
    text.delete('1.0', "end")
    text.insert('1.0', data)
def info():
    messagebox.showinfo("Information", "Friendly text editor")
def save():
    t = text.get('1.0', 'end')
    global file_name
    f = open(file_name, 'w')
    f.write(t)
    f.close()
file_name = tk.NONE
def new():
    global file_name
    file_name = "Untitled"
    text.delete('1.0', 'end')
root['bg'] = 'black'
root.title(file_name + ' | Friendly Text Editor')
root.minsize(width = 500, height = 600)
root.maxsize(width = 600, height = 700)
text = tk.Text(root, width = 100, height = 100)
scroll = tk.Scrollbar(root, orient = 'vertical', command = text.yview)
scroll.pack(side = 'right', fill = 'y')
text.configure(yscrollcommand=scroll.set)
text.pack(side = 'left', padx = 5, pady=5)
menuBar = tk.Menu(root)
fileMenu = tk.Menu(menuBar)
fileMenu.add_command(label = "New", command = new)
fileMenu.add_command(label = "Open", command = open_file)
fileMenu.add_command(label = "Save", command = save)
fileMenu.add_command(label = "Save as", command = save_as)
menuBar.add_cascade(label = "File", menu=fileMenu)
menuBar.add_cascade(label = "Info", command = info)
menuBar.add_cascade(label = "Exit", command = root.quit)
root.config(menu=menuBar)
text.insert('1.0','\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n')
root.mainloop()

